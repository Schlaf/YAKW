#include <Wire.h>
#include <SPI.h>
#include <Adafruit_PN532.h>
#include <Keyboard.h>
//#include <require_cpp11.h>

//define the pins for SPI communication.
#define PN532_SCK  (0)
#define PN532_MISO (1)
#define PN532_MOSI (2)
#define PN532_SS   (3)


// Use this line for a breakout with a software SPI connection (recommended):
Adafruit_PN532 nfc(PN532_SCK, PN532_MISO, PN532_MOSI, PN532_SS);

// Use this line for a breakout with a hardware SPI connection.  Note that
// the PN532 SCK, MOSI, and MISO pins need to be connected to the Arduino's
// hardware SPI SCK, MOSI, and MISO pins.  On an Arduino Uno these are
// SCK = 13, MOSI = 11, MISO = 12.  The SS line can be any digital IO pin.
//Adafruit_PN532 nfc(PN532_SS);

// Or use this line for a breakout or shield with an I2C connection:
//Adafruit_PN532 nfc(PN532_IRQ, PN532_RESET);

uint8_t slot = 0;
void setup(void) {
  Serial.begin(115200);

  nfc.begin();

  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    Serial.print("Didn't find PN53x board");
    while (1); // halt
  }
  // configure board to read RFID tags
  nfc.SAMConfig();
}

void loop(void) {
  
  if (Serial.available()){
    uint8_t inputByte = Serial.read();
    switch (inputByte){
      case 'w'://write
        Serial.println("Attempting write.");
        if (writeToChip()){
          Serial.println("Page written.");
        }
        break;
      case 's'://select
        if (slotSelect()){
          Serial.print("Selected slot: "); Serial.println(slot);
          }else{
          Serial.println("Slot select failed.");
        }
        break;
      case 'd'://dump
        dumpTag(); 
        break;
      default:
        //Serial.print(inputByte); Serial.print(" "); delay(10);
        break;
    }
  }else{
    uint8_t success = 0;
    uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
    uint8_t uidLength = 0;                    // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
      
    // Wait for an NTAG chip.  When one is found 'uid' will be populated with
    // the UID, and uidLength will indicate the size of the UUID (normally 7)
    success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength, 1000);
    
    if (success) {
      // Display some basic information about the card
      Serial.println("Found an ISO14443A card");
      Serial.print("  UID Length: ");Serial.print(uidLength, DEC);Serial.println(" bytes");
      Serial.print("  UID Value: ");
      nfc.PrintHex(uid, uidLength);
      Serial.println("");
        if (uidLength == 7){
          // We probably have an NTAG2xx card (though it could be Ultralight as well)
          Serial.println("Seems to be an NTAG2xx tag (7 byte UID)");
          readSlot();
      }else {
          Serial.println("This doesn't seem to be an NTAG203 tag (UUID length != 7 bytes)!");
        }
    }
  }
}

bool slotSelect(){
  uint8_t tempVar = 0;
  uint8_t inputByte = 0;
  for (uint8_t n=0; n<3; n++){//loop thru decimal digits
    if(!Serial.available()){
      Serial.println("not available");
      return(0);
    }
    inputByte = Serial.read();
    //turn from ASCII to decimal value
    inputByte = inputByte - 48;
    //invalid user input
    if (!(inputByte >= 0 && inputByte <= 9)){
      Serial.print("InputByte range check");
      Serial.println(inputByte);
      return(0);
    }
    //multiply to add correct number of 0's
    //integer overflows may occur. I don't want to think about handing those.
    inputByte = inputByte*pow(10, 2-n);

    //add the three decimal numbers together
    tempVar = tempVar + inputByte;
  }
  //make sure the no invalid slot is selected
  if (tempVar > 54){
    Serial.println("Password slot may not be higher than 54.");
    return(0);
  }
  memcpy(&slot, &tempVar, 1);
  return(1);
}

bool writeToChip(){
  uint8_t pageNo = 0;
  uint8_t success = 0;
  /*This code actually works diffrently, depending on whether writeBuff[] is initialized or not.
  I always knew memory safety was an issue in c++, 
  but I never realized it was ** THIS ** bad.
  Using c++ has caused me to realize, how much of a mess this language is.
  I thought rust people were weird; now I am one of them.
  What the actual fuck, c++!?*/
  uint8_t writeBuff[4] = {0,0,0,0};
  uint8_t compareBuff[4] = {0,0,0,0};
  uint8_t inputByte = 0;
  //read user input for write
  for (uint8_t i=0; i<5; i++){ //number of "bytes"
    for (uint8_t n=0; n<3; n++){//loop thru decimal digits
      if(!Serial.available()){
        Serial.println("not available");
        return(0);
      }
      inputByte = Serial.read();
      //turn from ASCII to decimal value
      inputByte = inputByte - 48;
      //invalid user input
      if (!(inputByte >= 0 && inputByte <= 9)){
        Serial.print("InputByte range check");
        Serial.println(inputByte);
        return(0);
      }
      //multiply to add correct number of 0's
      //integer overflows may occur. I don't want to think about handing those.
      inputByte=inputByte*pow(10, 2-n);
      if (i==0){//Page number
        //add the three decimal numbers together
        pageNo = pageNo + inputByte;
        //make sure the lock bytes are not written to
        if (pageNo < 4 or pageNo > 225){
          Serial.print("PageNo range check");
          return(0);
        }
      }else{
        //add the three decimal numbers together
        writeBuff[i-1] = writeBuff[i-1] + inputByte;
      }
    }
  }
  success = nfc.ntag2xx_WritePage(pageNo, writeBuff);
  if (success){
    //check if the write really worked.
    //the return code of the NFC library is not always accurate.
    nfc.ntag2xx_ReadPage(pageNo, compareBuff);
    for (uint8_t i = 0; i<4; i++){
      if (writeBuff[i] != compareBuff[i]){
        //write might have failed
        Serial.print("Failed to write to page ");
        Serial.println(pageNo);
        return(0);
      }
    }
    return(1);
  }else{
    //write might have failed
    Serial.print("Failed to write to page ");
    Serial.println(pageNo);
    return(0);
  }
}

void readSlot(){
  uint8_t success;
  uint8_t readBuff[4] = {0,0,0,0};
  //rawPasswd must be 16 bytes long.
  //The typePassword function does not perform a range check for this.
  //typePassword is probably not memory safe for values lower than 16.
  uint8_t rawPasswd[16];
    
  //success = nfc.mifareultralight_SectorSelect(1, data);
  //Serial.print("Sector select success: "); Serial.println(success);
  //nfc.PrintHexChar(data, 4);
  // TAG Type       PAGES   USER START    USER STOP
  // --------       -----   ----------    ---------
  // NTAG 216       231     4             225
  for (uint8_t i = 0; i < 4; i++){
    uint8_t page = 222-slot+i;
    success = nfc.ntag2xx_ReadPage(page, readBuff);
    // Display the results, depending on 'success'
    if (success) {
      // Dump the page data
      memcpy(&rawPasswd[i*4], &readBuff, 4);
      //nfc.PrintHexChar(&rawPasswd[i*4], 4);
    }else{
      // Display the current page number
      Serial.print("PAGE ");
      Serial.print(page);
      Serial.print(": ");
      Serial.println("Unable to read the requested page!");
      nfc.PrintHexChar(readBuff, 4);
      return;
    }
  }
  typePassword(rawPasswd); 
}

void typePassword(uint8_t *rawPasswd) {
  const uint8_t symbols[] = {'q','w','e','r','t','u','i','o','p','a','s','d','f','g','h','j','k','l','x','c','v','b','n','m',',','.',' ','Q','W','E','R','T','U','I','O','P','A','S','D','F','G','H','J','K','L','X','C','V','B','N','M','1','2','3','4','5','6','7','8','9','0','!','$','%'};
  uint8_t shiftBuffer[2] = {0,0};
  Keyboard.begin();
  //get all symbols except for the last one. The last one is only 2 bits.
  for (uint8_t i=0; i < 21; i++){
    uint8_t passwdByte = (i*6)/8;
    if (passwdByte < 15){
      memcpy(&shiftBuffer[0], &rawPasswd[passwdByte], 2);
    } else {
      //try not to copy more Bytes, than the Password has
      memcpy(&shiftBuffer[0], &rawPasswd[passwdByte], 1);
    }
    //the switch block puts the ID of the symbol, that should be typed in shiftBuffer[0]
    //trinket M0 is small endian
    switch ((i*6)%8){ //bit mask start
      case 0:
        //drop last 2 bits, ignore second byte
        shiftBuffer[0] = shiftBuffer[0] >> 2;
        break;
      case 2:
        //drop the first 2 bits, ignore second byte
        shiftBuffer[0] = shiftBuffer[0] << 2;
        shiftBuffer[0] = shiftBuffer[0] >> 2;
        break;
      case 4:
        //discard first 4 bits, combine with last 2 bits
        shiftBuffer[0] = shiftBuffer[0] << 4;
        shiftBuffer[0] = shiftBuffer[0] >> 2;
        shiftBuffer[1] = shiftBuffer[1] >> 6;
        shiftBuffer[0] = shiftBuffer[0] | shiftBuffer[1];
        break;
      case 6:
        //discrad first 6 bits, combine with first 4 bits
        shiftBuffer[0] = shiftBuffer[0] << 6;
        shiftBuffer[0] = shiftBuffer[0] >> 2;
        shiftBuffer[1] = shiftBuffer[1] >> 4;
        shiftBuffer[0] = shiftBuffer[0] | shiftBuffer[1];
        break;
      default:
        Serial.println("Somebody touched the Math, REEEEEEEEEEEEEEEEEEEEEEEEEE!!!!");
        break;
    }
    if (shiftBuffer[0] < 64){
      //Serial.print(symbols[shiftBuffer[0]]);
      Keyboard.write(symbols[shiftBuffer[0]]);
    } else {
      Serial.println("amogus"); //error handling
    }
    delay(25); //typing speed issue in HyperV?
  }
  delay(50);
  //get the last symbol
  memcpy(&shiftBuffer[0], &rawPasswd[15], 1);
  shiftBuffer[0] = shiftBuffer[0] << 6;
  shiftBuffer[0] = shiftBuffer[0] >> 6;
  //Serial.println(symbols[shiftBuffer[0]]);
  Keyboard.write(symbols[shiftBuffer[0]]);
  Keyboard.write(KEY_RETURN);
  Keyboard.end();
  // Don't spam the password
  delay(3000);
}

void dumpTag(){
  uint8_t success = 0;
  uint8_t data[4] = {0,0,0,0};
  for (uint8_t i = 0; i < 230; i++){
        success = nfc.ntag2xx_ReadPage(i, data);
        
        // Display the current page number
        Serial.print("PAGE ");
        if (i < 10){
          Serial.print("0");
          Serial.print(i);
        }
        else{
          Serial.print(i);
        }
        Serial.print(": ");

        // Display the results, depending on 'success'
        if (success){
          // Dump the page data
          nfc.PrintHexChar(data, 4);
        }
        else{
          Serial.println("Unable to read the requested page!");
        }
      }
}
