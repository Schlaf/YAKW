# Yet Another Keyboard Wedge (YAKW)

## This project is a work in progress and still somewhat rough around the edges.

This repo contains an Arduino sketch, that can read and write passwords from an NTAG I2C chip. The wedge will type the password as a USB Keyboard. This means it does not require a driver, and can be used for pre-boot authentication.  
This wedge is intended to work with both american qwerty and german qwertz layouts. In order to avoid having to switch between layouts while using the Wedge, the passwords only consist of the following 64 symbols: `qwertuiopasdfghjklxcvbnm,. QWERTUIOPASDFGHJKLXCVBNM1234567890!$%`.  
Each password is stored in a 6 bit encoding and occupies 4 pages in memory. This gives each password 128bit equivalent complexity.

## How to use

The arduino sketch was written for the atsamd21X family of microcrontrollers and the PN532 NFC chip.  It has been tested with the Adafruit trinket M0 and the Seeeduino XIAO microcontrollers.

### Installation

1. Clone this repo into a folder named YAKW.
`git clone https://gitlab.com/Schlaf/YAKW.git YAKW`

2. Install the adafruit PN532 Library.

3. Change line 137 in Adafruit_PN532.cpp to: `spi_dev = new Adafruit_SPIDevice(ss, clk, miso, mosi, 50000,`. This is necesserary, to set the correct clock speed fot the SPI connection between the trinket and PN532. I might change this to use I2C in the future, to avoid this issue.

4. Compile and flash.

### Serial commands

The follwing serial commands are supported:  

`d` = dump  
This will dump the memory contents of a tag to serial.  

`s` = select password  
This command will select a different password. The number of the password must be given as three decimal digits. To select password number 3, your would issue command `s003`.

`w` = write  
This command will write to a page on the Tag.  
It takes five 8 bit integers as arguments. They must be typed out in decimal notation.  
The first integer is the page number. The following four integers are the byte values.  
To write the bytes `0x69 0x69 0x69 0x69` to page 50, you would issue the command `w050105105105105`.  

Make sure to use zero padding, so that each integer provided as am argument is three digits long.
