import secrets, serial, time

whatever = ['q','w','e','r','t','u','i','o','p','a','s','d','f','g','h','j','k','l','x','c','v','b','n','m',',','.',' ','Q','W','E','R','T','U','I','O','P','A','S','D','F','G','H','J','K','L','X','C','V','B','N','M','1','2','3','4','5','6','7','8','9','0','!','$','%']
for i in whatever:
    print(i,end="")

time.sleep(3)
port = serial.Serial('/dev/ttyACM0')
for n in range(100,226):
    testvar = "w" + str(n).zfill(3)
    command = []
    for i in range(4):
        command.append(str(secrets.randbits(8)))
        testvar = testvar + str(command[i]).zfill(3)
    print(testvar)
    #testvar = "s002"
    port.write(bytes(testvar, 'ASCII'))
    print(port.readline())

port.close()

"""
port = serial.Serial('/dev/ttyACM0')
print(port.name)
testvar = "s000"
port.write(bytes(testvar, 'ASCII'))
print(port.read(20))

port.close()
"""
"""
print('{' ,end='')
for i in range(16):
    print(str(secrets.randbits(8)) + " ,",end='')
print('}')

for i in range(22):
    print((i*6) % 8)
"""